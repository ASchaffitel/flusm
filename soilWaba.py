# -*- coding: utf-8 -*-
'''
=======================
soil module of FluSM
=======================
functions for calculating the soil water balance. Called during the execution of the module FluSM.py

'''
import numpy as np
import pandas as pd 
from scipy.optimize import curve_fit
import sys

#import scipy.stats as stats


def hydraulicConduct_bbc(sat_eff, ks, B):
    """
    Burdine-Brooks-Corey model of the unsaturated hydraulic conductivity
    Method of Famingliettie & Wood (1994), Brocca et al. (2008), s. Brooks & Corey (1964) eq. 13
    
    Parameters:
    -----------
    sat_eff: pd.dataFrame 
        effective saturation
    ks: float
        saturated hydraulic conductivity 
    B: float
        pore size distribution index
    
    Returns: 
    ---------
    unsaturated hydraulic conductivity
    """
    eta=2./B+3.
    q=ks*sat_eff**eta
    return q



def get_drainageLoss(et0_d, soiMoi_ts, wetDryCycles, thresh_et0=0.5, theta_s=37., theta_r=2.):
    """
    Function for deriving the drainage loss [vol.%] for days with low potential evaporation.
    For these days, it is assumed that no soil evaporation takes place 
    and the observed change of soil moisture is only due to drainage losses
    
    Parameters:
    ------------
    et0_d: pd.Series
        time series of potential evaporation [mm/day]
    soiMoi_ts: pd.Series
        soil moisture time series [vol. %] with hourly temporal resolution
    wetDryCycles: pd.DataFrame
        time series of wet and dry cycles. 
        Must contain the column event_ind (0 for dry period and 1 for wet period)
        and the column wetDryCycle (consecutively numbered wet dry cycles. Each cycle starts with the rain event and ends before the next rain event starts)
    theta_s: float (optional). Default 37%
        saturated water content [vol.%]
    theta_r: float (optional). Default 2%
        residual water content [vol.%]
    thresh_et0: float (optional). Default 0.5 mm
        threshold for selecting days where no evaporation took place [mm/day]
        For days with et0 below this threshold it is assumed that no evaporation took place 
    
    Returns:
    -----------
    pd.DataFrame with the for times which meet the conditions: dry period and et0<thresh_et0
    with the columns:
        wc: soil moisture [vol.%]
        sat_eff: effective saturation [%]
        drainageLoss: drainage loss [vol.%]
    """
    days_noEvap=et0_d[(et0_d<thresh_et0)].index.date
    df=pd.DataFrame({'wc':soiMoi_ts, 'sat_eff':(soiMoi_ts-theta_r)/(theta_s-theta_r)})
    df['drainageLoss']=soiMoi_ts.diff(-1)[(wetDryCycles['event_ind']==0) & (np.isin(soiMoi_ts.index.date, days_noEvap))]   #diff(-1) --> prediction for next time step
    
    #------------------------------------------------------------------------------ 
    # prevent the calculation of the drainage loss over subsequent wetDryCycles
    lastEntry_dryPeriod=wetDryCycles[wetDryCycles['wetDryCycle'].shift(1)!=wetDryCycles['wetDryCycle']].index
    df.loc[np.isin(df.index, lastEntry_dryPeriod), 'drainageLoss']=np.NaN 
    
    return df.dropna(subset=['drainageLoss'])
    

def fit_drainageModel(drainLoss, allObs_drainModel='yes'):
    """
    function for fitting the parameters of the drainage model (unit gradient approach, unsaturated hydraulic conductivty with the Burdine-Brooks-Corey model)
    inversely to the observed drainage loss
    
    Parameters:
    ------------
    drainLoss: pd.DataFrame
        dataFrame returned from function get_drainageLoss. Must contain the columns [wc, sat_eff, drainageLoss] for times where drainage is the only process
        leading to a change of soil moisture within the soil profile.
        column wc: observed soil water content [vol.%]
        column sat_eff: effective saturation [-]
        column draingeLoss: observed change of soil moisture [vol.%] (assumed to be caused only by drainage)
    allObs_drainModel: yes/no (optional). Default: yes
        determines of all observations within drainLoss are used for fitting the drainage model
        yes: all observations are used
        no: only observations where drainLoss is positive are used for fitting (decreasing soil moisture)
    bootstrap: boolean (optional). Default: True
        whether the confidence intervals of the regression is determined by bootstrapping or from the covariance matrix of the regression
    bootstrap_runs: int (optional). Default: 5000
        number of runs for bootstapping the confidence intervals
        has no effect if bootstrap is False
    return_lastBootstrapParam: boolean (optional). Default False
        if both params bootstrap and return_lastBootstramParmam are True, ks and B of the last bootstrap run are returned instead
        of ks and B determined for the original drain loss dataFrame
        has no effect if bootstrap is False
    
    
    Returns:
    -----------
    A dictionary with the following keys:
        ks: fitted ks parameter of the drainage model
        B: fitted B parameter of the drainage model
        ci_ks: confidence interval of the fit for ks
        ci_B: confidence interval of the fit for B
        noObs: number of obersvations used for fitting
        RMSE: root mean squared error of the fit (quality criteria)
        MAE: mean absolute error of the fit (quality criteria)
        rSquared: r squared (quality criteria)
    """
    if allObs_drainModel=='no':
        drainLoss.loc[drainLoss['drainageLoss']<0,'drainageLoss']=np.NaN
    elif allObs_drainModel=='yes':
        pass
    else:
        sys.exit('wrong input for parameter allObs_drainModel (soilWaba.fit_drainageModel)\n(valid inputs are yes and no)')
    drainLoss.dropna(inplace=True, how='any')
 
    try:
        fitParams, fitCov=curve_fit(hydraulicConduct_bbc, drainLoss.loc[:,'sat_eff'], drainLoss.loc[:,'drainageLoss'])#, bounds=([-np.inf, 0], [np.inf, 100]))
    except RuntimeError:
        fitParams, fitCov =np.array([np.NaN, np.NaN]), np.array([[np.NaN, np.NaN],[ np.NaN, np.NaN]])
    except ValueError:
        fitParams, fitCov =np.array([np.NaN, np.NaN]), np.array([[np.NaN, np.NaN],[ np.NaN, np.NaN]])
     
    ks=fitParams[0]
    B=fitParams[1]
     
    sigma_ks, sigma_B=np.sqrt(np.diag(fitCov))
     
    simulated=drainLoss['sat_eff'].apply(hydraulicConduct_bbc, ks=ks, B=B)
    residuals=drainLoss['drainageLoss']-simulated
     
    RMSE=((residuals**2.).sum()/(residuals.shape[0]-2))**.5
    MAE=residuals.abs().mean()
     
    ss_res=np.sum(residuals**2)
    ss_tot=np.sum((drainLoss.loc[:,'drainageLoss']-np.mean(drainLoss.loc[:,'drainageLoss']))**2)
    rSquared=1-(ss_res/ss_tot)
    
    return {'ks':ks, 'B':B, 'sigma_ks':sigma_ks, 'sigma_B':sigma_B, 'RMSE':RMSE, 'MAE': MAE}
    



def predict_drainLoss(soiMoi_ts, drainModel, theta_s=37., theta_r=2):
    """
    Function for predicting the drainage loss from the state of the soil water content
    Note that the predicted drainage loss will be constraint during the closure of the soil water balance (function calc_soilWaba)
    
    Parameters:
    -----------
    soiMoi_ts: pd.Series
        soil moisture time series [vol.%] with a temporal resolution of 1h
    drainModel: dict
        parameters of the fitted drainage model, as returned from the function fit_drainageModel
    theta_s: float (optional). Default 37%
        saturated water content [vol.%]
    theta_r: float (optional). Default 2%
        residual water content [vol.%]
    
    Returns:
    ----------
    predicted drainage loss [vol.%/h]
    """
    sat_eff=((soiMoi_ts-theta_r)/(theta_s-theta_r)).dropna()
    drainPredict=hydraulicConduct_bbc(sat_eff, ks=drainModel['ks'], B=drainModel['B']).reindex(pd.date_range(soiMoi_ts.index[0], soiMoi_ts.index[-1], freq='h')).shift(1)
    return drainPredict
    
def calc_soilWaba(soiMoi_ts, drainPredict, sSurf_state, wetDryCycles, constraint1='on', constraint2='on'):
    """
    function for calculating the soil water balance
    
    Parameters:
    ------------
    soiMoi_ts: pd.Series
        soil moisture time series [vol.%] with a temporal resolution of 1h
    sSurf_state: pd.Series
        time series of the state of the surface storage [mm] with a temporal resolution of 1 hour 
    wetDryCycles: pd.DataFrame
        time series of wet and dry cycles. 
        Must contain the column event_ind (0 for dry period and 1 for wet period)
        and the column wetDryCycle (consecutively numbered wet dry cycles. Each cycle starts with the rain event and ends before the next rain event starts)
    constaint1: on or off (optional). Default on
        whether the soil water balance is constrained to the condition that no soil evaporation takes place while water is available in the surface storage
    constaint2: on or off (optional). Default on
        whether the soil water balance is constrained to the condition that drainage during dry periods is limited to the observed change of soil moisture
    """
  
    wbSoil_df=pd.DataFrame(data={'wc': soiMoi_ts, 'dWc': soiMoi_ts.diff(1), 'drainageLoss':drainPredict})

    mask_infiltr=-1*wbSoil_df['dWc']<wbSoil_df['drainageLoss']
    wbSoil_df['infiltrGains']=wbSoil_df.loc[mask_infiltr, 'drainageLoss']+wbSoil_df.loc[mask_infiltr, 'dWc']
    wbSoil_df['infiltrGains'].fillna(0, inplace=True)

    mask_evapo=-1*wbSoil_df['dWc']>wbSoil_df['drainageLoss']
    wbSoil_df['evapoLoss']=-1*wbSoil_df.loc[mask_evapo, 'dWc']-wbSoil_df.loc[mask_evapo, 'drainageLoss']
    wbSoil_df['evapoLoss'].fillna(0, inplace=True)
    
    #------------------------------------------------------------------------------ 
    # model constraint: no evaporation as long as the surface storage is not empty
    if constraint1=='on':
        noEvap_mask=(sSurf_state>0.01)&(wbSoil_df['infiltrGains']==0.)
        wbSoil_df.loc[noEvap_mask,'drainageLoss']=-1*wbSoil_df.loc[noEvap_mask, 'dWc']#+=wbSoil_df.loc[noEvap_times,'evapoLoss']
        wbSoil_df.loc[noEvap_mask,'evapoLoss']=0
    elif constraint1=='off':
        pass
    else:
        sys.exit('wrong input for constraint1 of wbSoil.calc_soilWaba\nvalid inputs are on and off')

    #------------------------------------------------------------------------------ 
    # model constraint: drainage loss during dry periods can not exceed the observed soil moisture change (no infiltration during dry periods)
    # infiltration only possible during rain events + the succeeding 3h of the next dry period
    if constraint2=='on':
        soiMoi_dry=soiMoi_ts.loc[wetDryCycles[wetDryCycles['event_ind']==0].index].to_frame()
        soiMoi_dry['time_UTC']=soiMoi_dry.index
        times_noInf=soiMoi_dry.groupby(wetDryCycles['wetDryCycle']).apply(lambda x: x.iloc[3:,]['time_UTC']).tolist()
    
        mask=(soiMoi_ts.index.isin(times_noInf)) & (wbSoil_df['infiltrGains']>0)
        wbSoil_df.loc[mask, 'drainageLoss']=-1*wbSoil_df.loc[mask, 'dWc']#-=wbSoil_df.loc[mask, 'infiltrGains']
        wbSoil_df.loc[mask, 'infiltrGains']=0
    elif constraint2=='off':
        pass
    else:
        sys.exit('wrong input for constraint2 of wbSoil.calc_soilWaba\nvalid inputs are on and off')

    wbSoil_df.loc[wbSoil_df['dWc'].isna(),:]=np.NaN
    return wbSoil_df
    
    