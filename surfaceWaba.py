# -*- coding: utf-8 -*-
'''
=======================
surface module of FluSM
=======================
functions for calculating the surface water balance. Called during the execution of FluSM.py

'''
import numpy as np
import pandas as pd

def calc_dSsurf_dt(Ssurf_plot, rain_10m, wetDryCycles, et0_h):
    """
    function for calculation of the change of the surface storage over time
    
    Parameters:
    ------------
    Ssurf_plot: float
        surface storage capacity [mm] of the plot
    rain_10m: pd.Series
        precipitation time series [mm/10min]
        Name of the time series must be 'prec'
    wetDryCycles: pd.DataFrame
        must contain the column event_ind (0 for dry period and 1 for wet period)
        and the column wetDryCycle (consecutively numbered wet dry cycles. Each cycle starts with the rain event and ends before the next rain event starts)
    et0_h: pd.Series
        time series of the potential evaporation [mm/h]
        
    Returns:
    -----------
    time series (pd.Series) of the change of the surface storage [mm/10min]
    """

    rainEvents_10m=pd.concat([rain_10m, wetDryCycles.resample('10min').ffill(5)], axis=1)
    rainEvents_10m['time_UTC']=rainEvents_10m.index
    
    #------------------------------------------------------------------------------ 
    # flux into surface storage
    rain_cs=rainEvents_10m.groupby('wetDryCycle')['prec'].cumsum()
    sSurf_flux=pd.Series(np.zeros(len(rainEvents_10m)), index=rainEvents_10m.index)
    sSurf_flux.loc[rain_cs<=Ssurf_plot]=rainEvents_10m.loc[rain_cs<=Ssurf_plot, 'prec']
    sSurf_state=sSurf_flux.groupby(rainEvents_10m['wetDryCycle']).cumsum()
    precCsAboveSsurf=rainEvents_10m[rain_cs>Ssurf_plot].groupby('wetDryCycle')['time_UTC'].nth(0).tolist()
    sSurf_flux[precCsAboveSsurf]=Ssurf_plot-sSurf_state[precCsAboveSsurf]
    
    #------------------------------------------------------------------------------ 
    # state of surface storage determined from filling of storage and ET0 of previous event
    et0_h.loc[et0_h<0]=0                        #no condesation
    et0_h.loc[wetDryCycles['event_ind']==1]=0   #no evaporation during rainfall
    et0_10m=et0_h.resample('10min').bfill(5)/6.
    et0_cs=et0_10m.groupby(rainEvents_10m['wetDryCycle']).cumsum()
    
    states=[]
    ssurf_prev=0
    for name, group in sSurf_flux.groupby(rainEvents_10m['wetDryCycle']):
        sSurf_state=group.cumsum()+ssurf_prev
        sSurf_state[sSurf_state>Ssurf_plot]=Ssurf_plot
        
        et0_grp_cs=et0_cs[sSurf_state.index]
        
        sSurf_state-=et0_grp_cs
        sSurf_state[sSurf_state<0]=0
        
        ssurf_prev=sSurf_state.iloc[-1]
        states.append(sSurf_state)
    
    sSurf_stateTs=pd.concat(states)
    return sSurf_stateTs.diff(1)



def calc_ro(dSsurf_dt, rain_10m, icap):
    """
    function for calculation of the surface runoff
    
    Parameters:
    ------------
    dSsurf_dt: pd.Series
        time series of the change of the surface storage of the plot [mm/10min]
    rain_10m: pd.Series
        precipitation time series with a temporal resolution [mm/10min]
        Name of the time series must be 'prec'
    icap: float
        infiltration capacity [mm/h] of the plot
    
    Returns:
    -----------
    time series of surface runoff [mm/10min]
    """
    ro_10m=pd.Series(np.zeros(len(rain_10m)), index=rain_10m.index)
    sSurf_fill=dSsurf_dt.copy()
    sSurf_fill.loc[sSurf_fill<0]=0
    
    rain_effective=rain_10m-sSurf_fill
    ro_10m.loc[rain_effective>icap/6.]=rain_10m.loc[rain_effective>icap/6.]-icap/6.
    return ro_10m

def calc_infiltr(dSsurf_dt, rain_10m, icap):
    """
    function for calculating the infiltration into the soil
    
    Parameters:
    ------------
    dSsurf_dt: pd.Series
        time series of the change of the surface storage of the plot [mm/10min]
    rain_10m: pd.Series
        precipitation time series [mm/10min]
        Name of the time series must be 'prec'
    icap: float
        infiltration capacity [mm] of the plot
    
    Returns:
    -----------
    time series of infiltration [mm/10min]
    
    """
    infiltr_10m=pd.Series(np.zeros(len(rain_10m)), index=rain_10m.index)
    sSurf_fill=dSsurf_dt.copy()
    sSurf_fill.loc[sSurf_fill<0]=0
        
    rain_effective=rain_10m-sSurf_fill
    infiltr_10m.loc[rain_effective>=icap/6.]=icap/6.
    infiltr_10m.loc[rain_effective<icap/6.]=rain_effective[infiltr_10m<icap/6.]
    
    return infiltr_10m
        


